<?php
require_once 'machines.php';

$token = "DEV";
$name = "Beta open";
$curl = curl_init();
require_once 'machines.php';
require_once 'saveStateByUid.php';
require_once 'session.php';

$user = getUser();
$machine =  $machine_details[$_GET["door_id"]];
$payload = json_encode([
    "token" => $machine["token"],
    "name" => $user['name']
]);

// Prepare new cURL resource
$ch = curl_init($m["ip"].'users/closewithtime/');
$ch>curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
curl_setopt($ch, CURLOPT_MAXREDIRS, 10);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);

// Set HTTP Header for POST request
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Content-Length: ' . strlen($payload))
);

// Submit the POST request
$result = curl_exec($ch);
$err = curl_error($ch);
// Close cURL session handle
curl_close($ch);

if ($err) {
    echo "dest $machine";
    echo "cURL Error #:" . $err;
} else {
    $r = json_decode($result, True);
    if (empty($r["error"]) && empty($r["warning"])) {
        $outFileSaved = saveStateByUid($machine_details[$_GET["door_id"]]["uid"], $result, $machine_details);
        if ($outFileSaved) {
            echo $result;
        } else {
            echo json_encode([
                "error" => "File save failed",
                "newState" => null,
                "remoteOpenUntil" => null,
            ]);

        }
    } else if (!empty($r["warning"]) && $r["warning"] === "No active open until date.") {
        $r["openUntil"] = null;
        $r["newState"] = "locked";
        $newResult = json_encode($r);
        $outFileSaved = saveStateByUid($machine_details[$_GET["door_id"]]["uid"], $newResult, $machine_details);
        if ($outFileSaved) {
            echo $newResult;
        } else {
            echo json_encode([
                "error" => "File save failed",
                "newState" => null,
                "remoteOpenUntil" => null,
            ]);

        }
    } else {
        echo $result;
    }
}

