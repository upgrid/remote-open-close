<?php

$syncFrom =  filter_input($_GET["syncFrom"], FILTER_VALIDATE_INT);
$curl = curl_init();


curl_setopt_array($curl, array(
    CURLOPT_PORT => "8000",
    CURLOPT_URL => "http://10.8.0.238:8000/users/update/tags/".$syncFrom,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "postman-token: 4a945930-3d91-a4c3-53e8-d6247090c58d"
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    echo $response;
}
