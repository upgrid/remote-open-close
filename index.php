<?php require_once 'machines.php'; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://use.fontawesome.com/3b0232839f.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <title>Upgrid remote control</title>
    <style>
        th {
            text-align: center;
        }
    </style>
    <script>
        var doors_count = <?php echo count($all_machines)?>;
    </script>
    <script src="main.js"></script>
    <style>
        th, tr {
            text-align: center;
        }

        .doorColumn {
            width: 150px;
        }

        #mytable tr.row:hover td
        {
            border-style: double;
            border-color: red;
        }
    </style>

</head>
<body>
<div>
    <b>Door will be open for: </b>
    <?php $openTimes = [
            5 => "5s",
            10 => "10s",
            30 => "30s"
        ];
        $openTimes[(5 * 60)] = "5m";
        $openTimes[(10 * 60)] = "10m";
        $openTimes[(30 * 60)] = "30m";
        for ($o = 1; $o <= 5; $o += 0.5) {
            $optionText = (explode(".", $o)[0]).'h';
            if (substr($o, -2) === ".5") {
                $optionText.=' 30m';
            }
            $openTimes[($o*3600)] = $optionText;
        }
        ksort($openTimes);
    ?>
    <select name="time" id="door-open-time">

            <?php foreach ($openTimes as $key => $value) : ?>
                <option value="<?php echo $key ?>"><?php echo $value ?></option>
            <?php endforeach ?>
            </select>
</div>
<br>
<table border="1" id="mytable">

    <tr>
        <th><b>Name</b></th>
        <?php for ($i = 0, $l = count($all_machines); $i < $l; $i++) : ?>
            <th class="doorColumn">
                <div onclick="openDoorBlockByColor(<?php echo $i?>)">
                    <?php
                    $m = $all_machines[$i];
                    echo strpos($m, 'local')  ? explode('.', $m)[0] : $m;
                    ?>
                </div>
            </th>
        <?php endfor ?>
    </tr>
    <tr>
        <td><b>Status</b></td>
        <?php for ($i = 0, $l = count($all_machines); $i < $l; $i++) : ?>
            <td class="doorColumn">

                <div class="btn btn-default">
                    <i
                        class="fa fa-5x fa-lock"
                        id="status-<?php echo $i ?>"
                        aria-hidden="true"
                        onclick="clickHandler(<?php echo $i ?>)"
                    ></i>
                </div>
            </td>
        <?php endfor ?>
    </tr>
    <tr>
        <td><b>Open until</b></td>
        <?php for ($i = 0, $l = count($all_machines); $i < $l; $i++) : ?>
            <td class="doorColumn">
                <div id="openUntil-<?php echo $i ?>" onclick="closeDoorBlockByColor(<?php echo $i?>)"></div>
            </td>
        <?php endfor ?>
    </tr>
</table>
</body>
</html>