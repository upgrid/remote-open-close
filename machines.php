<?php
/**
 * Created by PhpStorm.
 * User: valdur55
 * Date: 22.11.16
 * Time: 10:50
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once 'session.php';
$showMachines = [4];
$machine_details = getDoorsNameByIds($showMachines);

$all_machines =[
];

foreach ($machine_details as $m) {
    $all_machines[] = $m["name"];
}


function searchForId($id, $array) {
    foreach ($array as $key => $val) {
        if ($val['uid'] === $id) {
            return $array[$key];
        }
    }
    return null;
}
