var status_elements = [];
var dest_url = window.location.href.split("/");
dest_url.pop();
dest_url = dest_url.toString().replace(/,/g,"/");
status_update_timeout = false;
first = true;

function calculateOpenuntil(door_id){
	return document.getElementById("door-open-time").value
}

function setToken(xhr) {
	xhr.setRequestHeader("X-Access-Token", JSON.parse(window.localStorage.getItem("session")).token)
}

function openDoor(door_id) {
	first = false;
	var data = new FormData();

	var xhr = new XMLHttpRequest();
	xhr.withCredentials = true;

	xhr.addEventListener("readystatechange", function () {
		if (this.readyState === 4) {

			response = this.responseText;
			if (response.indexOf('not allowed.') !== -1) {
				setStatus('disabled', door_id)
			}

			if (response.indexOf('cURL Error') !== -1) {
			    setStatus('error', door_id)
            }

			if (response.includes("Door will be open until")) {
				setStatus('unlocked', door_id)
				alert(xhr.responseText)
			}

			let r = JSON.parse(this.responseText);

			if (r["newState"] === "unlocked") {
				setStatus('unlocked', door_id)
				setOpenUntil(r["openUntil"], door_id)
			} else {
				if (r["warning"]) {
					alert(r["warning"])
				} else {
					alert(r["error"])
					setStatus('error', door_id)
				}
			}

			/*
			 status_update_timeout = setTimeout(function(){
			 clearInterval(status_update_interval);
			 status_update_interval = false;
			 }, 15000)
			 */
		}
	});
	openUntil = calculateOpenuntil(door_id);
	xhr.open("GET", dest_url+"/open.php?door_id=" + door_id + "&openTime=" + openUntil);
	setToken(xhr);

	xhr.send(data);

}


function clickHandler(door_id) {
	let stat = changeLockColor(door_id)
	if (!stat) {
		return true;
	}

	stat.classList.contains("fa-lock") ? openDoor(door_id) : closeDoor(door_id);

}

function changeLockColor(door_id) {
	let stat = document.getElementById("status-"+door_id);

	switch (stat.style["color"]) {
		case "red":
		case "blue":
			return false;
		default:
			stat.style["color"] = "blue"
			return stat;
	}
}

function closeDoorBlockByColor(door_id) {
	let stat = changeLockColor(door_id)
	if (!stat) {
		return true;
	}

	closeDoor(door_id);

}

function openDoorBlockByColor(door_id) {
	let stat = changeLockColor(door_id)
	if (!stat) {
		return true;
	}

	openDoor(door_id);
}

function closeDoor(door_id) {
	first = false;
	var data = new FormData();
	var xhr = new XMLHttpRequest();
	xhr.withCredentials = true;

	xhr.addEventListener("readystatechange", function () {
		if (this.readyState === 4) {
			response = this.responseText;
            if (response.indexOf('not allowed.') !== -1) {
                setStatus('disabled', door_id)
            }

            if (response.indexOf('cURL Error #:Could not resolve host:') !== -1) {
                setStatus('error', door_id)
            }

            let r = JSON.parse(response);

			if (r["newState"] === "locked") {
				setStatus('locked', door_id)
				setOpenUntil(r["openUntil"], door_id)
				setTimeout(() => {
					if (r["message"]) {
						alert(r["message"])
					}
				}, 60)

			} else {
				if (r["warning"]) {
					if (r["warning"] === "No active open until date.") {
						setStatus("locked", door_id)
					}
					alert(r["warning"])
				} else {
					if (alert(r["error"])) {
						alert(r["error"])
					}
					setStatus('error', door_id)
				}
			}
			/*
			 status_update_timeout = setTimeout(function(){
			 clearInterval(status_update_interval);
			 status_update_interval = false;
			 }, 15000)
			 */
		}
	});

	openUntil = calculateOpenuntil(door_id);
	xhr.open("GET", dest_url+"/close.php?door_id=" + door_id + "&timestamp=" + openUntil);
	setToken(xhr);

	xhr.send(data);

}

function setStatus(status, door_id) {
	var door = "status-"+door_id;
	var stat = document.getElementById(door);
	if (stat) {
		switch (status) {
			case "unlocked":
			case "locked":
				stat.style["color"] = "black";
				var opposite = status === "locked" ? "unlock" : "lock";
				var same = status === "locked" ? "lock" : "unlock";
				stat.classList.remove("fa-" + opposite);
				stat.classList.add("fa-"+ same);
                stat.style["color"] = same ===  "unlock" ? 'green' : 'black';
				break;
			case 'disabled':
				stat.style["color"] = "yellow";
				break;
			default:
				stat.style["color"] = "red";
				break;
		}
	}




}

function setOpenUntil(openUntil, door_id) {
    var door = "openUntil-"+door_id;
    var stat = document.getElementById(door);
    stat.innerHTML = openUntil;
}



function getStatus(){
	if(status_update_interval) {
		clearInterval(status_update_interval);
		status_update_interval = false;

	}

	var update_doors = [];

	for (var i = 0;  i < doors_count; i++) {
		var door = "status-" + i;
		var stat = document.getElementById(door);
		if (stat.style["color"] === "red") {
			continue;
		}
		update_doors.push(i);
	}

	if (update_doors.length === 0)
		return;

	var xhr = new XMLHttpRequest();
	xhr.withCredentials = true;
	var data = new FormData();

	xhr.addEventListener("readystatechange", function () {

		if (this.readyState === 4) {

			var response = JSON.parse(this.responseText);

			for (var i = 0; i < response.length; i++) {
				var status = response[i];
				setStatus(status["status"], parseInt(status["door_id"]));
                setOpenUntil(status["openUntil"] || null, parseInt(status["door_id"]));
			}




			if (status_update_timeout) {
				clearTimeout(status_update_timeout);

			}

			status_update_timeout = setTimeout(getStatus, 1000)
		}

	});


	xhr.open("GET",
		dest_url + "/status.php?doors=" + update_doors.join(',')
	);
	setToken(xhr);
	xhr.send(data);

}
status_update_interval = setInterval(getStatus, 5000);