<?php

header("Content-Type: application/json; charset=UTF-8");
$_POST = json_decode(file_get_contents('php://input'), true);

$all_machines = [];
$machine_details = [];

require_once 'machines.php';
require_once 'saveStateByUid.php';
require_once 'session.php';



$machine =  getDoorByToken();

if ($machine) {
    $outFileSaved = saveStateByUid($machine["id"], json_encode([
        "newState" => "locked",
        "openUntil" => null
    ]), $machine_details);

    if (!$outFileSaved) {
        echo json_encode([
            "ERROR" => "File save failed",
        ]);
    } else {
        echo json_encode([
            "result" => "OK",
        ]);
    }
}



