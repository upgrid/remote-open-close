<?php

function getConn() {
    $servername = "localhost";
    $username = "ookean";
    $password = "ookean";
    $dbname = "ookean";

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    return $conn;
}

function prepared_query($mysqli, $sql, $params, $types = "")
{
    $types = $types ?: str_repeat("s", count($params));
    $stmt = $mysqli->prepare($sql);
    $stmt->bind_param($types, ...$params);
    $stmt->execute();
    return $stmt;
}

function prepared_select($mysqli, $sql, $params = [], $types = "") {
    return prepared_query($mysqli, $sql, $params, $types)->get_result();
}

function prepared_select_in_array($mysqli, $sql, $params = [], $whereFrom = 'id') {
    $clause = implode(',', array_fill(0, count($params), '?')); //create 3 question marks
    $types = str_repeat('i', count($params)); //create 3 ints for bind_param
    $sql.=" WHERE $whereFrom IN ($clause);";
    return prepared_query($mysqli, $sql, $params, $types)->get_result();
}



function getUser() {
    $conn = getConn();

    $sql = "SELECT users.id, concat(first_name, ' ',  last_name) as name FROM users JOIN usersessions ON usersessions.user_id = users.id WHERE token = ?";

    $result = prepared_select($conn, $sql, [$_SERVER["HTTP_X_ACCESS_TOKEN"]], "s");
    $user = $result->fetch_assoc();

    return $user;
}

function getDoorByToken() {
    $conn = getConn();

    $sql = "SELECT doors.id, name FROM doors JOIN doorsessions ON doorsessions.door_id = doors.id WHERE token = ?";

    $result = prepared_select($conn, $sql, [$_SERVER["HTTP_X_ACCESS_TOKEN"]], "s");
    $door = $result->fetch_assoc();

    return $door;
}

function getDoorNameById($id) {
    $conn = getConn();

    $sql = "SELECT doors.id, name FROM doors WHERE id = ?";

    $result = prepared_select_in_array($conn, $sql, $id, "i");
    $door = $result->fetch_assoc();

    return $door;
}


function getDoorsNameByIds($ids = []) {
    $conn = getConn();

    $sql = "SELECT doors.id as uid, name, token, ip FROM doors JOIN doorsessions ON doorsessions.door_id = doors.id";

    $result = prepared_select_in_array($conn, $sql, $ids, "doors.id");
    $doors = $result->fetch_all(MYSQLI_ASSOC);

    return $doors;
}
?>