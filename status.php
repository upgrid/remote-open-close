<?php

require_once 'machines.php';
require_once 'session.php';

getUser();

function get_door($uid) {
    $stat = "status/".$uid;
    $status = json_decode(file_get_contents($stat), TRUE);
    return $status;
}

$update_doors = isSet($_REQUEST['doors']) ? explode(',' , $_REQUEST['doors']) :  [];
$result = [];
foreach ($update_doors as $door) {
    $result[$door]= get_door($machine_details[$door]["uid"]);
    $result[$door]["door_id"] = $door;
    $result[$door]["status"] = $result[$door]["newState"];
}

echo json_encode($result);
