<?php require_once 'machines.php'; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://use.fontawesome.com/3b0232839f.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <title>Upgrid remote control</title>
    <style>
        th {
            text-align: center;
        }
    </style>
    <script>
        var doors_count = <?php echo count($all_machines)?>;
    </script>
    <script src="main.js"></script>

</head>
<body>
<table border="1">

    <tr>
        <?php for ($i = 0, $l = count($all_machines); $i < $l; $i++) : ?>
            <th><?php
                $m = $all_machines[$i];
                echo strpos($m, 'local')  ? explode('.', $m)[0] : $m;
                ?></th>
        <?php endfor ?>
    </tr>
    <tr>
        <?php for ($i = 0, $l = count($all_machines); $i < $l; $i++) : ?>
            <th><select name="time" id="cars">
                    <?php for ($i = 0, $l = count($all_machines); $i <= 5; $i += 0.5) : ?>
                        <option value="$i">$i</option>
                    <?php endfor ?>
                </select></th>
        <?php endfor ?>
    </tr>
    <tr>
        <?php for ($i = 0, $l = count($all_machines); $i < $l; $i++) : ?>
            <td>

                <div class="btn btn-default">
                    <i
                        class="fa fa-5x fa-lock"
                        id="status-<?php echo $i ?>"
                        aria-hidden="true"
                        onclick="clickHandler(<?php echo $i ?>)"
                    ></i>
                </div>
            </td>
        <?php endfor ?>
    </tr>
    <tr>
        <?php for ($i = 0, $l = count($all_machines); $i < $l; $i++) : ?>
            <td id="changed-<?php echo $i ?>">
            </td>
        <?php endfor ?>
    </tr>
</table>
</body>
</html>